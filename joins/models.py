from django.db import models

# Create your models here.

class Join(models.Model):
	email = models.EmailField()
	# Foreign key to itself!
	friend = models.ForeignKey("self", related_name="referral", \
								null=True, blank=True)
	ref_id = models.CharField(max_length=120, default='ABC', unique=True)
	ip_address = models.CharField(max_length=120, default='ABC')
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __str__(self):
		return self.email

	class Meta:
		unique_together = ('email', 'ref_id')

#class JoinFriends(models.Model):
#	email = models.OneToOneField(Join, related_name="Sharer")
#	friends = models.ManyToManyField(Join, related_name="Friend", \
#									null=True, blank=True)
#	emailall = models.ForeignKey(Join, related_name="emailall")
#
#	def __str__(self):
#		print("friends are ", self.friends.all())
#		print("emailall ", self.emailall)
#		print("email ", self.email)
#cm		return self.email.email

#1) Install south: pip install south, add south to settings.py in INSTALLED_APPS
#2) Ensure Model is in the synced section in database
#3) Convert the model to south with: manage.py convert_to_south <appname>
#4) Make changes to model (eg add new fields: ip_address = models.CharField(max_length=120, default='ABC'))
#5) Run schemamigration: manage.py schemamigration <appname> --auto
#6) Run migrate: manage.py migrate

#OBS.: django 1.7 comes with south